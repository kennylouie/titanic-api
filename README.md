# Titanic API

## Background

Blog post for the full description of this project is found at https://kennylouie.gitlab.io.

API built in with Go using a machine trained model from R. The data was trained from the Kaggle Titanic dataset. Can predict the survival of passengers providing a list of the passenger's features:

Live demo can be curled:

```
curl -k -X POST -d '{"sex": "female", "parch": "0", "age": "26", "fare": "7.925", "pclass": "3", "sibsp": "0", "embarked": "S", "passengerid": "3"}' http://ec2-34-214-104-142.us-west-2.compute.amazonaws.com/predict
```

## Dependencies

+ goscore
+ gorilla/handlers
+ gorilla/mux
+ godotenv

``` bash
go get github.com/gorilla/handlers
go get github.com/gorilla/mux
go get github.com/joho/godotenv
go get github.com/asafschers/goscore
```
