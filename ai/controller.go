package ai

import (
  "io/ioutil"
  "net/http"
  "encoding/json"
  "io"
  "log"
)

type Controller struct {}

func (c *Controller) Predict(w http.ResponseWriter, r *http.Request) {
  var features Features
  body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

  log.Println(body)

  if err != nil {
    log.Fatalln("Error Predict", err)
    w.WriteHeader(http.StatusInternalServerError)
    return
  }

  if err := r.Body.Close(); err != nil {
    log.Fatalln("Error Predict", err)
  }

  if err := json.Unmarshal(body, &features); err != nil {
    w.WriteHeader(422)
    log.Println(err)
    if err := json.NewEncoder(w).Encode(err); err != nil {
      log.Fatalln("Error Predict Unmarshalling data", err)
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
  }

  log.Println(features)
  result := TraversePMML(features)
  if len(result) == 0 {
    w.WriteHeader(http.StatusInternalServerError)
      return
  }

  w.Header().Set("Content-Type", "application/json; charset=UTF-8")
  w.WriteHeader(http.StatusOK)
  w.Write(result)
  return
}
