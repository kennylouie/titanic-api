package ai

type Features struct {
  PassengerId int `json:"PassengerId,string"`
  Age int `json:"Age,string"`
  Sex string  `json:"Sex"`
  Parch int `json:"Parch,string"`
  Fare float64 `json:"Fare,string"`
  Pclass  int `json:"Pclass,string"`
  SibSp int `json:"SibSp,string"`
  Embarked  string `json:"Embarked"`
}
