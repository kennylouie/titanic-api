package ai

import (
  "github.com/asafschers/goscore"
  "io/ioutil"
  "encoding/xml"
)

func TraversePMML(features Features) []byte {
  modelXml, err := ioutil.ReadFile("titanic_rf.pmml")
  if (err != nil) {
    panic(err)
  }

  var model goscore.RandomForest
  xml.Unmarshal([]byte(modelXml), &model)

  featureSet := map[string]interface{}{
    "Sex": features.Sex,
    "Parch": features.Parch,
    "Age": features.Age,
    "Fare": features.Fare,
    "Pclass": features.Pclass,
    "SibSp": features.SibSp,
    "Embarked": features.Embarked,
    "PassengerId": features.PassengerId,
  }

  score, _ := model.LabelScores(featureSet)

  if score["0"] > score["1"] {
    return []byte("Did not survive")
  } else {
    return []byte("Survived")
  }

}
