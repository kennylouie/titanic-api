package main

import (
  "log"
  "net/http"
  "os"
  "github.com/gorilla/handlers"
  "gitlab.com/kennylouie/titanic-api/ai"
  _ "github.com/joho/godotenv/autoload"
)

func main() {

  port := os.Getenv("PORT")
  if port == "" {
    log.Fatal("$PORT must be set")
  }

  router := ai.NewRouter()

  allowedOrigins := handlers.AllowedOrigins([]string{"*"})
  allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})

  log.Fatal(http.ListenAndServe(":" + port, handlers.CORS(allowedOrigins, allowedMethods)(router)))

}
